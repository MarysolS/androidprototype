package com.altomobile.marysol.androidprototype;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class ListViewAdapter extends BaseAdapter {
    private static LayoutInflater inflater;
    Context context;
    ArrayList<String> days;

    public ListViewAdapter(Context context, ArrayList<String> days){
        this.context = context;
        this.days = days;
        this.inflater = (LayoutInflater) this.context.getSystemService(this.context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {

        return this.days.size() ;
    }

    @Override
    public Object getItem(int position) {

        return this.days.get(position);
    }

    @Override
    public long getItemId(int position) {

        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final View view = this.inflater.inflate(R.layout.list_element, null);
        TextView dayTextView = view.findViewById(R.id.dayTextView);

        dayTextView.setText(this.days.get(position));
        return view;
    }
}
