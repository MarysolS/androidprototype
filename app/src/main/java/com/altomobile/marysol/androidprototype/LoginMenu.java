package com.altomobile.marysol.androidprototype;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import java.util.Arrays;

public class LoginMenu extends AppCompatActivity {
    LoginButton loginBtn;
    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_menu);

        if(this.isLoggedIn()){
            this.redirectToListView();
        }

        this.callbackManager = CallbackManager.Factory.create();
        this.loginBtn = findViewById(R.id.loginBtn);

        this.setLoginBtn();

    }

    private void setLoginBtn(){
        this.loginBtn.setReadPermissions(Arrays.asList("email"));

        this.loginBtn.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                if(loginResult.getRecentlyDeniedPermissions().contains("email")){
                    Toast.makeText(getApplicationContext(), "You have to grant the Facebook email permission." , Toast.LENGTH_LONG).show();
                }else{
                    redirectToListView();
                }
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException exception) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }


    private void redirectToListView(){
        startActivity(new Intent(LoginMenu.this, ListViewMenu.class));
    }

    public boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();

        if(accessToken == null || accessToken.getDeclinedPermissions().contains("email")){
           return false;
        }

        return true;
    }
}
