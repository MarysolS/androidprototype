package com.altomobile.marysol.androidprototype;

import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListViewMenu extends AppCompatActivity {
    int numberOfDays;
    ListView daysListView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view_menu);

        this.numberOfDays = 31;
        this.daysListView = findViewById(R.id.daysListView);

        if(getApplication().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            this.adjustListViewHeight();
        }

        this.setListView();
    }

    private void setListView(){
        ArrayList<String> days = new ArrayList<>();
        for(int i = 1; i <= this.numberOfDays; i++){
            days.add("Day " + i);
        }

        ListViewAdapter listViewAdapterAdapter = new ListViewAdapter(this, days);
        this.daysListView.setAdapter(listViewAdapterAdapter);

        this.daysListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(), (String) parent.getItemAtPosition(position), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void adjustListViewHeight(){
        DisplayMetrics displayMetrics = getApplication().getResources().getDisplayMetrics();
        int screenHeight = Math.round(displayMetrics.heightPixels / displayMetrics.density);

        this.daysListView.getLayoutParams().height = screenHeight;
    }

}
